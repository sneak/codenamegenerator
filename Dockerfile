FROM python:3.8

ADD . /app

WORKDIR /app

ENV PYTHONPATH /app
ENV PORT 80

CMD [ "python3", "/app/bin/server" ]

EXPOSE 80
