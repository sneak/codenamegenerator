default: codename

codename:
	@PYTHONPATH=. python3 bin/codename

build:
	docker build -t sneak/codenamegenerator .
