# codenamegenerator

This is a python module that generates random codenames for projects.

A basic command, as well as a simple HTTP API server, are included.

I have intentionally excluded words that are long, confusing, hard to spell,
as well as occupations, animal names with negative connotations, and some
foods and body parts in an attempt to avoid randomly generating negative,
threatening, or disgusting code names.

This module uses nothing but the python3 standard library.

# Rationale

* [What M&A code names are and why investment banks use
  them](https://corporatefinanceinstitute.com/resources/knowledge/deals/project-names-in-investment-banking/)
* [WSJ: Project Funway: Code Names Help Spice Up the Art of the
Deal](https://www.wsj.com/articles/bankers-lament-loss-of-code-names-for-deals-1409279749)

# Run

Clone the repo, then run `PYTHONPATH=. python3 bin/server` inside it.

# Demo

An instance of `master` should be running at [https://codename.sneak.run/](https://codename.sneak.run/).

# See Also

* [docker
names-generator](https://github.com/moby/moby/blob/master/pkg/namesgenerator/names-generator.go)
* [Generate-Codename (perl, 2011)](https://github.com/sneak/Generate-Codename)

# Invitation

You are invited to contribute to this repository.  Pull requests that
introduce additional words for the lists are welcome, but please keep it
simple.

# Author

Jeffrey Paul &lt;sneak@sneak.berlin&gt;

https://sneak.berlin

[@sneakdotberlin](https://twitter.com/sneakdotberlin)

`5539 AD00 DE4C 42F3 AFE1  1575 0524 43F4 DF2A 55C2`

# License

This code is released into the public domain.
