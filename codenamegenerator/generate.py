#!/usr/bin/env python
#234567891123456789212345678931234567894123456789512345678961234567897123456789
# encoding: utf-8

import http.server
import json
import os
import random
import socketserver

# afaik this breaks if you install as an egg
this_dir_path = os.path.dirname(os.path.realpath(__file__))

FIRST = []

with open(os.path.join(this_dir_path, "first.txt"), 'r') as fp:
    FIRST = [ x.strip() for x in fp.readlines() ]

SECOND = []

with open(os.path.join(this_dir_path, "second.txt"), 'r') as fp:
    SECOND = [ x.strip() for x in fp.readlines() ]

def rand_firstword():
    return(random.choice(FIRST))

def rand_secondword():
    return(random.choice(SECOND))

def generate_codename():
    return(rand_firstword() + " " + rand_secondword())

def clientry():
    print(generate_codename())

class CodenameHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-Type',
            'application/json; charset=utf-8')
        self.end_headers()
        self.wfile.write(
            json.dumps(
                { 'codename': generate_codename() }
            ).encode('utf-8')
        )

def run_server():
    PORT = int(os.environ.get('PORT',8080))

    with socketserver.TCPServer(("", PORT), CodenameHandler) as httpd:
        print("serving codenames at port", PORT)
        httpd.serve_forever()
