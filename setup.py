from setuptools import setup

setup(
    name='codenamegenerator',
    version='1.0.0',
    author='Jeffrey Paul',
    author_email='sneak@sneak.berlin',
    packages=['codenamegenerator'],
    url='https://github.com/sneak/codenamegenerator/',
    license='Public Domain',
    description='Generates codenames',
    install_requires=[],
    entry_points={
        'console_scripts': [
            'generate-codename=generatecodename:clientry'
        ]
    },
)
